package com.nova.solutions.junit.mockito.example.junit.util;

import com.nova.solutions.junit.mockito.example.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@DisplayName("Util test Class")
public class UtilTest {

  @DisplayName("Get the age ok.")
  @Test
  void getAgeTestOk() throws Exception {
    String birthDate = "11/11/1995";
    Assertions.assertEquals(26, Util.getAge(birthDate));
  }

  @DisplayName("Get the age but throws error")
  @Test
  void getAgeTestBad() throws Exception {
    String birthDate = "11/11/2050";
    Assertions.assertThrows(RuntimeException.class, () -> Util.getAge(birthDate));
  }
  
  @DisplayName("Get the age from source")
  @ParameterizedTest
  @CsvSource({"11/11/1995, 26", "04/05/2011, 11", "27/07/1992, 29"})
  void getAgeFromSource(String birthDate, int age) throws Exception {
    Assertions.assertEquals(age, Util.getAge(birthDate));
  }
  
  @DisplayName("Get status with code 1")
  @Test
  void getStatusTest1() {
    int statusCode = 1;
    Assertions.assertEquals("Request", Util.getStatus(statusCode));
  }
  
  @DisplayName("Get status for all status code")
  @ParameterizedTest
  @CsvSource({"1,Request", "2,Pending", "3,In Process", "4, Complete", "9,N/A"})
  void getStatusTestAll(int statusCode, String status) {
    Assertions.assertEquals(status, Util.getStatus(statusCode));
  }
}
